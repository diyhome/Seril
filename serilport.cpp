#include "serilport.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <sstream>

SerilPort::SerilPort(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SerilPort)
{
    ui->setupUi(this);

    //loop find used serial
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        QSerialPort myCom;
        myCom.setPort(info);

        if(myCom.open(QIODevice::ReadWrite))
        {
            ui->seril_box->addItem(myCom.portName());
            myCom.close();
        }
    }
    //Initialize baudRate_box
    QStringList baudList,parityList,dataBitList,stopList;

    baudList<<"50"<<"75"<<"100"<<"134"<<"150"<<"200"<<"300"
           <<"600"<<"1200"<<"1800"<<"2400"<<"4800"<<"9600"
          <<"14400"<<"19200"<<"38400"<<"56000"<<"57600"
         <<"76800"<<"115200"<<"128000"<<"256000";

    parityList<<"无"<<"奇"<<"偶";
    parityList<<"标志";
    parityList<<"空格";

    dataBitList<<"5"<<"6"<<"7"<<"8";

    stopList<<"1"<<"1.5"<<"2";

    ui->send_btn->setEnabled(false);

    ui->baudrate_box->addItems(baudList);
    ui->baudrate_box->setCurrentIndex(12);

    ui->databit_box->addItems(dataBitList);
    ui->databit_box->setCurrentIndex(3);

    ui->parily_box->addItems(parityList);
    ui->parily_box->setCurrentIndex(0);

    ui->stopbit_box->addItems(stopList);
    ui->stopbit_box->setCurrentIndex(0);

    ui->characterchanged_bg->setId(ui->cc_rad,0);
    ui->characterchanged_bg->setId(ui->ch_rad,1);
    ui->characterchanged_bg->setId(ui->cd_rad,2);
    ui->characterchanged_bg->setId(ui->dc_rad,3);
}

SerilPort::~SerilPort()
{
    delete ui;
}

void SerilPort::on_send_btn_clicked()
{
    //myCom->write(ui->input_tex->toPlainText().toLatin1());
    myCom->write(ui->input_tex->text().toLatin1());
}

void SerilPort::on_clean_btn_clicked()
{
    ui->onput_tex->clear();
}

void SerilPort::on_ocseril_btn_clicked()
{
    myCom = new QSerialPort;
    myCom->setPortName(ui->seril_box->currentText());
    if(myCom->open(QIODevice::ReadWrite))
    {
        myCom->open(QIODevice::ReadWrite);
        qDebug() << tr("Serial INI Sucessful!");
        myCom->setBaudRate(ui->baudrate_box->currentText().toInt());

        switch (ui->databit_box->currentText().toInt()) {//Define Data Bits
        case 5:myCom->setDataBits(QSerialPort::Data5);break;
        case 6:myCom->setDataBits(QSerialPort::Data6);break;
        case 7:myCom->setDataBits(QSerialPort::Data7);break;
        case 8:myCom->setDataBits(QSerialPort::Data8);break;
        default:break;
        }

        //Define Flow Control
        myCom->setFlowControl(QSerialPort::NoFlowControl);

        switch (ui->parily_box->currentIndex()){//Define Parity
        case 0:myCom->setParity(QSerialPort::NoParity);break;
        case 1:myCom->setParity(QSerialPort::OddParity);break;
        case 2:myCom->setParity(QSerialPort::EvenParity);break;
        default:break;
        }

        switch (ui->stopbit_box->currentIndex()){//Define Stop Bit
        case 0:myCom->setStopBits(QSerialPort::OneStop);break;
        case 1:myCom->setStopBits(QSerialPort::OneAndHalfStop);break;
        case 2:myCom->setStopBits(QSerialPort::TwoStop);break;
        default:break;
            qDebug()<<tr("Serial Setting Finshed!");
        }

        ui->seril_box->setEnabled(false);
        ui->baudrate_box->setEnabled(false);
        ui->databit_box->setEnabled(false);
        ui->parily_box->setEnabled(false);
        ui->stopbit_box->setEnabled(false);

        //ui->ocseril_btn->setText(tr("Close"));
        ui->send_btn->setEnabled(true);
        ui->ocseril_btn->setEnabled(false);
        BTN_ISCLICKED = true;
        ui->actionClose_Serial->setEnabled(true);
        ui->actionSave->setEnabled(true);
        ui->actionClean_LCD->setEnabled(true);
        ui->actionScanning->setEnabled(false);

        //conncer
        QObject::connect(myCom,&QSerialPort::readyRead,this,&SerilPort::Read_Data_Com);
    }
    else
    {
        QMessageBox::about(NULL,tr("Prompt"),tr("Serial port is not open!"));
        return;
    }
}

void SerilPort::Read_Data_Com()
{
    QByteArray buf;
    buf = myCom->readAll();
    if (!buf.isEmpty())
    {
        //QString str = ui->onput_tex->toPlainText();
        //str+=tr(buf);
        //ui->onput_tex->clear();
        //ui->onput_tex->append(str);
    }
    buf.clear();
}

void SerilPort::on_actionClose_Serial_triggered()
{
    myCom->clear();
    myCom->close();
    myCom->deleteLater();

    ui->seril_box->setEnabled(true);
    ui->baudrate_box->setEnabled(true);
    ui->databit_box->setEnabled(true);
    ui->parily_box->setEnabled(true);
    ui->stopbit_box->setEnabled(true);
    ui->ocseril_btn->setEnabled(true);
    ui->actionClose_Serial->setEnabled(false);

    //ui->ocseril_btn->setText(tr("Open"));
    ui->send_btn->setEnabled(false);
    ui->actionScanning->setEnabled(true);
    //BTN_ISCLICKED = false;
}

void SerilPort::on_actionScanning_triggered()
{
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        QSerialPort myCom;
        ui->seril_box->clear();
        myCom.setPort(info);

        if(myCom.open(QIODevice::ReadWrite))
        {
            ui->seril_box->addItem(myCom.portName());
            myCom.close();
        }
    }
}

void SerilPort::INPUT_COMDATA(const QString get_DATA)
{
}
