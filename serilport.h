#ifndef SERILPORT_H
#define SERILPORT_H

#include <QMainWindow>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QStringList>
#include <QByteArray>
#include <QDebug>

namespace Ui {
class SerilPort;
}

class SerilPort : public QMainWindow
{
    Q_OBJECT

    //QSerialPort* myCom = new QSerialPort;

public:
    explicit SerilPort(QWidget *parent = 0);
    ~SerilPort();

private slots:
    void on_send_btn_clicked();

    void on_clean_btn_clicked();

    void on_ocseril_btn_clicked();

    void Read_Data_Com();

    void on_actionClose_Serial_triggered();

    void on_actionScanning_triggered();

private:
    Ui::SerilPort *ui;
    QSerialPort* myCom;
    //bool BTN_ISCLICKED = false;
    void INPUT_COMDATA(const QString get_DATA);
};

#endif // SERILPORT_H
