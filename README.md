# Seril
这个项目是我用来熟悉`C++` `UI`编程的项目，主要的用途就是串口的通讯

## myCom
使用`Qt Creator`的`QSerilPort`类实例的一个对象，用于对串口进行读写的操作
用该类的`QSerialPort::readyRead`信号构建了一个读取槽，实现不用`QTimer`实时读取串口数据

